# Remerciements

- Xavier Roca
- Florent Gallet
- Tiffanie Schenone
- Kévin Faucheur
- Laurent Schettini
- Sophie Roca
- Brice Montarou
- Benjamin Goutfer

- Christine Perony

# Diffusion et confidentialité

# Lexique

- ORM
- Framework

# Introduction

Pourquoi ce document + annonce du plan

# Presentation de l'entreprise

- presentation

## Produits & services
- SDI
- Sipleo
- Koomeo
- clients

## Resources humaines
- Pole administratif
- pole marketing & communication
- pole technique & support
- pole ingenieur R&D

# Vision de l'entreprise

## Une concurrence montante
- outils open source montant

## Un besoin changeant
- volonté des clients à avoir des logiciels "tout en un"

## La solution
- Cameline, extranet modulaire, ecosysteme complet pour entreprise (présentation globale)
    - Sipleo
    - Tiers
    - Credentials
    - Generateur de prix
    - Contrat
    - Notification
    - Logs

# Presentation du stage

## Contexte
- Stage M1
- Pourquoi SDI ?

## Objectifs
- le module contrat

## Integration dans l'équipe
- reunion de debut
- les pauses café
- ambiance familiales & entre-aide

## Présentation du projet
- contrainte technique
- Pour qui ? Api + Client web (ne pas exclure le portage mobile futur)
- Ou ? serveur client ou cloud entreprise
- Log
- Versioning & mise à jour
- Mise en place d'un Framework homemade
- International & regional (monnaie, date, langue ,etc...)

## Technologies utilisées
- Asp.net Core
- EF Core
- Microsoft SQL Server
- NLog
- Hang Fire
- Angular 6
- Ant

## Outils
- Visual Studio 2017
- Visual Studio Code
- PostMan
- Git
- Trello

# Réalisations

## Chronologie
- Aspect simple, dans quelle ordre et quel était le rôle de chacun

## Conception des modules

### Gestion des contrats
- tres grosse complexité
- Date, date et encore date
- souplesse abonnements & interventions
- modalité de paiement
- reconduction
- avenant
- permission d'edition selon etat

### Activités & Logs
- importance de traquer toutes actions
- garder trace à tout pris

- Amelioration de l'existant log base vers
    - log base (sync & async)
    - log file
    - log console coloré
    - Meilleure sémantique et adapté au RGPD ainsi qu'aux nouvelles technos

### Notification

- cron, mail, notif in app

## Sprints & Formations
- Winform
- EF 6 

- Net Core & EF Core
- Hang Fire
- NLog
- Ant & Angular 6

## Tests & Deploiements

# Apports

## Apports techniques

## Apports humains

#Conclusion